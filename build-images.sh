#!/usr/bin/env bash
set -e
fedora_version_list=("40")
java_version_list=("11" "17" "21")
gradle_version_list=("7.6.3" "8.4" "8.5" "8.6" "7.6.4" '8.7' '8.8' '8.9' '8.10.2')

to_semver() {
    if [[ "$1" =~ [0-9]\.[0-9]\.[0-9] ]]; then
      echo "$1"
    else
      echo "${1}.0"
    fi
}

for fedora_version in "${fedora_version_list[@]}"; do
  for java_version in "${java_version_list[@]}"; do
    for gradle_version in "${gradle_version_list[@]}"; do
      docker build -t "rbbl/kotlin-multiplatform-builder:gradle$(to_semver "$gradle_version")-jdk${java_version}-f${fedora_version}" \
        --build-arg "FEDORA_VERSION=${fedora_version}" \
        --build-arg "JDK_VERSION=${java_version}" \
        --build-arg "GRADLE_VERSION=${gradle_version}" \
        --target gradle \
        . ;
      docker push "rbbl/kotlin-multiplatform-builder:gradle$(to_semver "$gradle_version")-jdk${java_version}-f${fedora_version}" ;
      docker build -t "rbbl/kotlin-multiplatform-builder:gradle$(to_semver "$gradle_version")-jdk${java_version}-f${fedora_version}-rootless" \
        --build-arg "FEDORA_VERSION=${fedora_version}" \
        --build-arg "JDK_VERSION=${java_version}" \
        --build-arg "GRADLE_VERSION=${gradle_version}" \
        --target rootless \
        . ;
      docker push "rbbl/kotlin-multiplatform-builder:gradle$(to_semver "$gradle_version")-jdk${java_version}-f${fedora_version}-rootless" ;
    done
  done
done