# Kotlin Multiplatform Builder

a ready-made Image to build and test Kotlin Multiplatform Projects. Based on the [Fedora Image](https://hub.docker.com/_/fedora).

Contains:

- Firefox
- Chromium
- Java
- Gradle

## References

- [Gitlab](https://gitlab.com/rbbl/kotlin-multiplatform-builder)
- [Docker Hub](https://hub.docker.com/r/rbbl/kotlin-multiplatform-builder) 

## Why not the gradle image as base?
the gradle base image is available as ubuntu and alpine edition. \
the ubuntu version has the problem that the browsers don't work in containers since they force you to install them through snap.
the alpine version has the problem that node cannot be run through gradle 