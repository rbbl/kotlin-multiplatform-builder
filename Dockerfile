ARG FEDORA_VERSION
FROM fedora:$FEDORA_VERSION AS browser
RUN dnf in -y chromium firefox && dnf clean all
ENV CHROME_BIN="/usr/bin/chromium-browser"

FROM browser AS java
ARG JDK_VERSION
RUN dnf in -y java-${JDK_VERSION}-openjdk && dnf clean all
ENV JAVA_HOME="/etc/alternatives/jre"

FROM fedora:$FEDORA_VERSION AS gradle-extractor
RUN dnf in -y unzip
ARG GRADLE_VERSION
RUN curl -o gradle-${GRADLE_VERSION}.zip https://downloads.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
RUN unzip -d /opt/gradle gradle-${GRADLE_VERSION}.zip

FROM java AS gradle
ARG GRADLE_VERSION
COPY --from=gradle-extractor /opt/gradle /opt/gradle
ENV GRADLE_HOME=/opt/gradle/gradle-${GRADLE_VERSION}
ENV PATH=${GRADLE_HOME}/bin:${PATH}

FROM gradle AS rootless
RUN groupadd -g 1000 kotlin
RUN adduser -u 1000 -g 1000 kotlin
USER 1000:1000
